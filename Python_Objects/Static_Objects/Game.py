import pyray
import raylib

#  Инициализация пользовательских файлов
from Python_Objects.Special_Objects.GameInterface import Interface
from Python_Objects.Static_Objects.Related_To_NPC.queue_class import Queue
from Python_Objects.Static_Objects.Related_To_NPC.Generators.NpcGenerator import NpcGenerator
from Python_Objects.Static_Objects.Kingdom_resources import KingdomResources


class Game:
    def __init__(self, content: dict, window_width: int = 1600, window_height: int = 800):
        # Объекты системы
        self.interface = Interface(window_width, window_height)  # Объект класса interface
        self.content = content  # Словарь всех ресурсов

        # Объявление переменных
        self.objects = {'buffs': []}  # Словарь объектов с которыми взаимодейтвует игрок # Save
        self.time = 0  # Переменная времени (часы) # Save
        self.frames_counter = 0  # Переменная тиков # Save

        self.resources = KingdomResources(content=self.content)  # Ресурсы королевства

        # Объявление констант на день
        self.FRAMES_PER_TIME = 120  # Количество тиков в игровой еденице времени
        self.TIME_PER_DAY = 60  # Количество едениц времени в игровом дне
        self.day_count = 1

        # Переменные связанные с NPC
        self.__max_visitors = 11  # Максимальное количество действующих NPC в одно время
        self.__active_visitor_counter = 0
        self.__npc_generator = NpcGenerator(content)
        self.__day_count = 1
        self.objects['queue'] = Queue(self.__max_visitors)

        self.__NpcCooldown_const = 5 * 60  # Изначальное значение переменной ниже
        self.__NpcCooldown = 0  # Интервал между добавлением новых поситителей

        self.__last_result: dict = self.resources.resources
        self.is_not_paused = True  # Проверка того, что время идет

    def __add_ambassador(self):
        self.objects['queue'].add_visitor(self.__npc_generator.generate_ambassador(self.day_count))

    def update(self) -> int:  # Обновление переменных системы
        self.__active_visitor_counter = len(self.objects['queue'].queue)

        # Обновление времени
        self.is_not_paused = self.resources.is_game_paused()  # Проверка амбасадора
        self.__time_update()

        # Обновление очереди
        self.__add_new_visitors()
        self.objects['queue'].update()

        # Проверка взаимодействия с NPC
        self.__check_interaction()

        # Возвращение индекса сцены, на который нужно сменить
        return self.scene_change_events()

    def __check_interaction(self):
        self.objects['queue'].check_cursor()

    def __add_new_visitors(self):
        if self.__NpcCooldown > 0 and self.is_not_paused:
            self.__NpcCooldown -= 1

        if self.__NpcCooldown <= 0 and self.__max_visitors != self.__active_visitor_counter and self.is_not_paused:
            self.objects['queue'].add_visitor(self.__npc_generator.generate_npc())
            self.__NpcCooldown = self.__NpcCooldown_const
            self.__active_visitor_counter += 1

    def __time_update(self) -> None:
        if self.is_not_paused:  # Проверка переменной-флага паузы времени
            self.frames_counter += 1

            if self.frames_counter == self.FRAMES_PER_TIME:  # В одной единице игрового времени frames_per_time тиков
                self.time += 1
                self.frames_counter = 0

            if self.time == self.TIME_PER_DAY:  # Проверка на окончание дня
                self.time = 0
                self.__day_end()

    def check_lose_conditions(self) -> bool:
        return self.resources.check_game_over_conditions()

    def scene_change_events(self) -> int:
        scene_index = 1
        if pyray.is_key_down(pyray.KeyboardKey.KEY_P) or pyray.is_key_down(pyray.KeyboardKey.KEY_SPACE):
            scene_index = 4

        if not self.check_lose_conditions():
            scene_index = 10

        return scene_index

    def __day_end(self) -> None:  # Активируется по окончании дня
        self.day_count += 1
        self.objects['queue'].clear_queue()
        self.__add_ambassador()
        self.is_not_paused = False
        self.resources.pause_game()
        self.resources.days_end_resource_reduction()

        self.TIME_PER_DAY += self.day_count  # Увеличение времени длительности дня

    def display(self) -> None:  # Отрисовка всех объектов
        bg = self.content['static_pictures']['main_game_bg']
        source_rect = pyray.Rectangle(0, 0, bg.width, bg.height)
        dest_rect = pyray.Rectangle(0, 0, self.interface.window_width, self.interface.window_height)
        pyray.draw_texture_pro(bg, source_rect, dest_rect, pyray.Vector2(0, 0), 0, raylib.WHITE)
        queue_result = self.objects['queue'].display()
        self.resources.display(queue_result)
