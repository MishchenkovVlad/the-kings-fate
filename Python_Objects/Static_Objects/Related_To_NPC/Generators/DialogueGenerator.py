import pyray
import raylib
from Python_Objects.Static_Objects.Related_To_NPC.Dialogue import Dialogue

from Python_Objects.Static_Objects.Related_To_Cards.CardLoader import *


class DialogueGenerator:
    def __init__(self, content: dict):
        self.__content: dict = content

    def generate_dialogue(self) -> Dialogue:
        card = load_card()
        options: list = load_options(card.event_id)
        title: str = card.title
        width: int = 900
        height: int = 600
        font: pyray.Font = self.__content['fonts']['default_font']
        font_size: int = 20
        dialogue_texture: pyray.Texture2D = self.__content['static_pictures']['scroll']
        color: pyray.Color = raylib.BLACK

        new_dialog = Dialogue(options=options, text=title, width=width, height=height, font=font,
                              font_size=font_size, dialogue_texture=dialogue_texture)

        return new_dialog

    def ambassador_dialogue(self, day_count) -> Dialogue:
        title: str = "Визит амбассадора."
        width: int = 900
        content_text = str(f"Сегодня {day_count} день вашего правления!")
        options = []
        height: int = 600
        font: pyray.Font = self.__content['fonts']['default_font']
        font_size: int = 35
        dialogue_texture: pyray.Texture2D = self.__content['static_pictures']['scroll']
        color: pyray.Color = raylib.BLACK

        new_dialog = Dialogue(options=options, text=title, width=width, height=height, font=font,
                              font_size=font_size, dialogue_texture=dialogue_texture, content_text=content_text)
        new_dialog.is_ambassador = True

        return new_dialog
