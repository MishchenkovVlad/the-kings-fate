import random
from Python_Objects.Static_Objects.Related_To_NPC.visitor_class import Visitor
from Python_Objects.Static_Objects.Related_To_NPC.Generators.DialogueGenerator import DialogueGenerator


class NpcGenerator:
    def __init__(self, content: dict):
        self.__content: dict = content
        self.__DialogueGenerator: DialogueGenerator = DialogueGenerator(content=content)

    def generate_npc(self, dialogue=None, sprite=None):
        if dialogue is None:
            dialogue = self.__DialogueGenerator.generate_dialogue()

        if sprite is None:
            sprite = self.__pick_sprite()
        if type(sprite) is str:
            if sprite == 'test':
                sprite = None

        new_npc = Visitor(-1, dialogue, sprite=sprite)

        return new_npc

    def generate_ambassador(self, day_count):
        sprite = self.__content['visitors']['ambassador']['ambassador']
        dialogue = self.__DialogueGenerator.ambassador_dialogue(day_count)
        return self.generate_npc(sprite=sprite, dialogue=dialogue)

    def __pick_sprite(self):
        visitor_types = list(self.__content['visitors'].keys())

        # Случайно выбираем тип посетителя
        chosen_type = random.choice(visitor_types)

        # Получаем список картинок для выбранного типа посетителя
        images = list(self.__content['visitors'][chosen_type].values())

        # Случайно выбираем картинку
        chosen_image = random.choice(images)

        return chosen_image
