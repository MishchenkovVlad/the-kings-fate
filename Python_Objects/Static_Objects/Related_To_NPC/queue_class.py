from raylib import MOUSE_BUTTON_LEFT, MOUSE_BUTTON_RIGHT

from Python_Objects.Static_Objects.Related_To_NPC.visitor_class import Visitor
from pyray import get_mouse_x, get_mouse_y, is_mouse_button_pressed


class Queue:
    def __init__(self, max_length):
        self.queue: list[Visitor] = list()
        self.__leaving_visitors: list[Visitor] = list()
        self.__max_length = max_length

    def add_visitor(self, visitor: Visitor):
        if len(self.queue) < self.__max_length:
            if visitor.pos_in_queue == -1:  # Если позиция -1, то добавляет в очередь нового посетителя в конец
                visitor.pos_in_queue = len(self.queue)
            self.queue.append(visitor)

    def update(self):
        visitor_index = 0
        while visitor_index < len(self.queue):
            visitor = self.queue[visitor_index]
            visitor.update()
            if visitor.waiting_time >= visitor.patient and visitor.is_ambassador:
                self.leave_visitor(visitor_index)
                visitor_index -= 1
            visitor_index += 1

        visitor_index = 0
        while visitor_index < len(self.__leaving_visitors):
            deletion = self.__leaving_visitors[visitor_index].update()
            if deletion:
                self.__leaving_visitors.pop(visitor_index)
                visitor_index -= 1
            visitor_index += 1

    def leave_visitor(self, index_of_leaving_visitor: int) -> None:
        self.queue[index_of_leaving_visitor].is_leaving = True
        self.__leaving_visitors.append(self.queue.pop(index_of_leaving_visitor))

        for i in range(index_of_leaving_visitor, len(self.queue)):
            self.queue[i].pos_in_queue -= 1

    def clear_queue(self):
        for i in range(len(self.queue)):
            self.leave_visitor(0)

    def check_cursor(self) -> None:
        mx = get_mouse_x()
        my = get_mouse_y()

        visitor = 0
        while visitor < len(self.queue):
            if self.queue[visitor].check_cursor(mx, my):
                if is_mouse_button_pressed(MOUSE_BUTTON_LEFT):
                    for v in self.queue:
                        v.change_show_dialogue(False)
                    self.queue[visitor].change_show_dialogue(True)
                elif is_mouse_button_pressed(MOUSE_BUTTON_RIGHT):
                    self.leave_visitor(visitor)
            visitor += 1

    def display(self):
        #  Отрисовка посетителей
        for el in self.queue:
            if not el.get_show_dialogue():
                el.display()

        #  Отрисовка недействующих посетителей
        for el in self.__leaving_visitors:
            el.display()

        #  Отрисовка посетителя с диалоговым окном
        for i in range(len(self.queue)):
            el = self.queue[i]

            if el.get_show_dialogue():
                result = el.display()
                if type(result) is dict:
                    self.leave_visitor(i)
                    return result
