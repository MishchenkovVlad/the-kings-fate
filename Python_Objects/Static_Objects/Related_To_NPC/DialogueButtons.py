from Dialogue import Dialogue
import raylib
import pyray

class Button:
    def __init__(self, text, x, y, width, height, color):
        self.text = text
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.color = color

    def draw(self):
        pyray.draw_rectangle(self.x, self.y, self.width, self.height, self.color)
        pyray.draw_text(self.text, self.x + 5, self.y + 5, 20, raylib.WHITE)

class DialogueWithButtons(Dialogue):
    def __init__(self, content: dict = dict(), text: str = '', width: int = 200, height: int = 300,
                 font_size: int = 50, color=raylib.BLACK, buttons=[], button_width=100, button_height=50):
        super().__init__(content, text, width, height, font_size, color)
        self.buttons = buttons
        self.button_width = button_width
        self.button_height = button_height

    def add_button(self, text):
        self.buttons.append(Button(text, self.x + self.width + 10, self.y + 5, self.button_width, self.button_height, raylib.GREEN))

    def display(self, x, y):
        super().display()
        for button in self.buttons:
            button.draw()