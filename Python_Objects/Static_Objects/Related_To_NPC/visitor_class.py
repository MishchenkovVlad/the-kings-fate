import pyray
import raylib
from time import time
from random import randint
from Python_Objects.Static_Objects.Related_To_NPC.Dialogue import Dialogue


class Visitor:
    def __init__(self, pos_in_queue: int, dialog: Dialogue, sprite=None):
        self.width = 150
        self.height = 250

        self.x = -self.width
        self.y = 500

        self.is_leaving = False
        self.y_shift_when_leaving = 40

        self.stand_pos_step = 20  # интервал между посетителями

        self.doorPos = 1000

        self.pos_in_queue = pos_in_queue

        self.start_waiting_time = time()
        self.waiting_time = 0
        self.patient = randint(32, 64)

        self.__dialogue = dialog
        self.__show_dialogue = False

        self.__dialogue_delta_x = -50
        self.__dialogue_delta_y = -100
        self.sprite = sprite

        self.is_ambassador = False

    def display(self):
        if self.sprite is None:
            pyray.draw_rectangle(self.x, self.y + self.is_leaving * self.y_shift_when_leaving,
                                 self.width, self.height,
                                 raylib.BLACK)
            if not self.is_leaving:
                pyray.draw_text(str(self.pos_in_queue), self.x + 25, self.y + 50, 100, raylib.PURPLE)
            else:
                pyray.draw_text("L", self.x + 25, self.y + 75, 100, raylib.PURPLE)
            print(self.x, self.y)
        else:
            dest_rect = pyray.Rectangle(self.x, self.y + 75 * self.is_leaving, self.width, self.height)
            source_rect = pyray.Rectangle(0, 0, self.sprite.width, self.sprite.height)
            pyray.draw_texture_pro(self.sprite, source_rect, dest_rect, pyray.Vector2(0, 0), 0, raylib.WHITE)

        if self.__show_dialogue:
            result = self.__display_dialogue()
            if type(result) is dict:
                return result

    def check_cursor(self, mx, my) -> bool:
        return (self.x <= mx <= self.x + self.width) and (self.y <= my <= self.y + self.height)

    def update(self) -> bool:
        self.is_ambassador = self.__dialogue.is_ambassador
        if self.is_ambassador:
            stop_waiting_time = time()
            self.waiting_time = stop_waiting_time - self.start_waiting_time

        if not self.is_leaving and self.x != self.doorPos - (self.stand_pos_step + 75) * self.pos_in_queue:
            self.x += 1
        elif self.is_leaving and self.x > -self.width:
            self.x -= 1
        else:
            return True
        return False

    def __display_dialogue(self):
        result = self.__dialogue.display()
        if result == 'close':
            self.change_show_dialogue(False)
        elif type(result) is dict:
            return result

    def change_show_dialogue(self, value: bool):  # ВКЛ/ВЫКЛ диалога
        self.__show_dialogue = value

    def get_show_dialogue(self):
        return self.__show_dialogue
