import pyray
import raylib


class Dialogue:
    def __init__(self, options: list = list, text: str = '', width: int = 600, height: int = 300, font_size: int = 45,
                 font: pyray.Font = pyray.get_font_default(), dialogue_texture=None, content_text: str = ''):
        self.__options = options
        self.__text = text
        self.__content_text = content_text
        self.__width = width
        self.__height = height
        self.__font_size = font_size
        self.__font = font
        self.__dialogue_texture = dialogue_texture
        self.X = 150
        self.Y = 5
        self.__choice: bool = True
        self.is_ambassador = False

    def display(self):
        texture_rect = pyray.Rectangle(0, 0, 1680, 1323)
        dialogue_rect = pyray.Rectangle(self.X-100, self.Y-100, self.__width+200, self.__height+200)

        pyray.draw_texture_pro(self.__dialogue_texture, texture_rect, dialogue_rect,
                               pyray.Vector2(0, 0), 0, raylib.WHITE)

        text = self.__text

        content_text = ''  # Строка текста

        cur_line_counter = 0

        # Алгорит разбиения на строки
        for ind in range(len(self.__content_text.split())):
            content_text += self.__content_text.split()[ind] + ' '
            cur_line_counter += len(self.__content_text.split()[ind])
            if cur_line_counter >= 15:
                cur_line_counter = 0  # Обнуление длины строки
                content_text += '\n' * 4  # Переход на новую строчку

        #  Удваиваем отступы и убираем неправильные символы
        for i in range(len(text)-1):
            c = text[i]
            if c == '\n':
                text = text[:i-1] + '\n' + text[i:]

        #  Отображение текста
        pyray.draw_text_ex(self.__font, text,
                           pyray.Vector2(self.X + 15, self.Y + 75),
                           self.__font_size, 1, raylib.BLACK)
        pyray.draw_text_ex(self.__font, content_text,
                           pyray.Vector2(200, 300),
                           self.__font_size, 1, raylib.BLACK)
        mouse_x = pyray.get_mouse_x()
        mouse_y = pyray.get_mouse_y()

        if self.__choice:
            #  Кнопки выбора
            for i in range(len(self.__options)):
                option = self.__options[i]
                button_rect = pyray.Rectangle(self.X,
                                              self.Y + 300 + 70 * i,
                                              800, 60)
                title = option.title
                for j in range(len(title) - 1):
                    c = title[i]
                    if c == '\n':
                        title = title[:j - 1] + '\n' + title[j:]

                pyray.draw_rectangle_rec(button_rect, raylib.BEIGE)
                pyray.draw_text_ex(self.__font, title, pyray.Vector2(button_rect.x, button_rect.y),
                                   self.__font_size - 5, 1, raylib.BLACK)

                if pyray.is_mouse_button_pressed(raylib.MOUSE_BUTTON_LEFT) and pyray.check_collision_point_rec(
                        pyray.Vector2(mouse_x, mouse_y), button_rect):
                    self.__choice = False
                    self.__text = option.consequence
                    return {'army': option.army, 'alive': option.alive, 'contentment': option.contentment,
                            'health': option.health, 'religion': option.religion, 'treasury': option.treasury,
                            'game_is_not_paused': 0}

        #  Кнопка закрыть
        close_button_rect = pyray.Rectangle(self.X + self.__width - 65, self.Y + 15, 50, 50)
        pyray.gui_button(close_button_rect, "X")

        if self.is_ambassador:
            ok_button_rect = pyray.Rectangle((self.X + self.__width) // 2 - 50, self.Y - 15 + self.__height, 80, 50)
            pyray.gui_button(ok_button_rect, "Понял")
            if pyray.is_mouse_button_pressed(raylib.MOUSE_BUTTON_LEFT) and pyray.check_collision_point_rec(
                    pyray.Vector2(mouse_x, mouse_y), ok_button_rect):
                self.__choice = False
                return {'army': 0, 'alive': 0, 'contentment': 0,
                        'health': 0, 'religion': 0, 'treasury': 0, 'game_is_not_paused': 1}

        if pyray.is_mouse_button_pressed(raylib.MOUSE_BUTTON_LEFT) and pyray.check_collision_point_rec(
                pyray.Vector2(mouse_x, mouse_y), close_button_rect):
            return 'close'
