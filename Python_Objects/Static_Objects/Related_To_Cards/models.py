from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, ForeignKey
from sqlalchemy.orm import mapper, sessionmaker, DeclarativeBase, relationship


def create_connection():
    engine = create_engine('sqlite:///Python_Objects//Static_Objects//Related_To_Cards//Events.db', echo=False)
    Base.metadata.create_all(engine)
    return engine


class Base(DeclarativeBase):
    pass


class Era(Base):
    __tablename__ = "tbl_era"

    era_id = Column(Integer, primary_key=True)

    name = Column(String(50))
    fractions = relationship("Fraction", back_populates="era")


class Fraction(Base):
    __tablename__ = "tbl_fraction"

    fraction_id = Column(Integer, primary_key=True)

    era_id = Column(Integer, ForeignKey("tbl_era.era_id"))
    era = relationship("Era", back_populates="fractions")

    name = Column(String(50))

    events = relationship("Event", back_populates="fraction")


class Event(Base):
    __tablename__ = "tbl_event"
 
    event_id = Column(Integer, primary_key=True)
    title = Column(String(250))

    fraction_id = Column(Integer, ForeignKey("tbl_fraction.fraction_id"))
    fraction = relationship("Fraction", back_populates="events")
 
    options = relationship("Option", back_populates="event")


class Option(Base):
    __tablename__ = "tbl_option"
 
    option_id = Column(Integer, primary_key=True)

    event_id = Column(Integer, ForeignKey("tbl_event.event_id"))
    event = relationship("Event", back_populates="options")

    title = Column(String(100))
    army = Column(Integer)
    alive = Column(Integer)
    treasury = Column(Integer)
    health = Column(Integer)
    contentment = Column(Integer)
    religion = Column(Integer)

    consequence = Column(String(500))