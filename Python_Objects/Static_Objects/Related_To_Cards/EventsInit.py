﻿import json
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, ForeignKey, select, desc
from sqlalchemy.orm import sessionmaker
from models import *

engine = create_connection()

session = sessionmaker(bind=engine)

events = {}
with open('Events.json', 'r', encoding="utf-8-sig") as f:
    events = json.load(f)

with session() as db:
    for era_key in events:
        era = events[era_key]
        era_db = Era(name=era["name"])
        db.add(era_db)
        for fraction_key in era["fractions"]:
            fraction = era["fractions"][fraction_key]
            fraction_db = Fraction(name = fraction["name"])
            for event in fraction["events"]:
                event_db = Event(title = event["title"])
                for option in event["options"]:
                    option_db = Option(
                        title = option["title"],
                        army = option.get("army", 0),
                        alive = option.get("alive", 1),
                        treasury = option.get("treasury", 0),
                        health = option.get("health", 0),
                        contentment = option.get("contentment", 0),
                        religion = option.get("religion", 0),
                        consequence = option["consequence"]
                    )

                    event_db.options.append(option_db)
                    fraction_db.events.append(event_db)
                    era_db.fractions.append(fraction_db)
    db.commit()
