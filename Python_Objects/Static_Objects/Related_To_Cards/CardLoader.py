import random
import Python_Objects.Static_Objects.Related_To_Cards.models as models

engine = models.create_connection()

session = models.sessionmaker(bind=engine)


def load_card():
    with session() as db:
        events = db.query(models.Event).all()
        event = events[random.randint(0, len(events) - 1)]
        return event


def load_options(event):
    with session() as db:
        options = db.query(models.Option).filter(models.Option.event_id == event)

        return list(options)
