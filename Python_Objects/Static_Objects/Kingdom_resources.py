import Python_Objects.Special_Objects.LogPrinter as Log
import pyray
import raylib


class KingdomResources:  # Класс королевства и его ресурсов
    def __init__(self, content: dict):
        self.resources = {'army': 100, 'treasury': 1000,
                          'health': 50, 'contentment': 50, 'religion': 50, 'game_is_not_paused': True}
        self.content = content

    def is_game_paused(self) -> bool:
        return self.resources['game_is_not_paused']

    def pause_game(self) -> None:
        self.resources['game_is_not_paused'] = False

    def resource_changes(self, resource_name: str,
                         value: int) -> None:  # Уменьшение конкректного ресурса по его названию
        if value == 0:
            return None

        if resource_name in self.resources.keys() and resource_name not in ['game_is_not_paused']:
            if self.resources[resource_name] >= value:
                self.resources[resource_name] += value
                if value < 0:
                    Log.print_log(f'Resource {resource_name} reduced by {value}')
                else:
                    Log.print_log(f'Resource {resource_name} increased by {value}')
            else:
                Log.print_log(f'Lack of resource: {resource_name}, value of this resource equated to 0')
                self.resources[resource_name] = 0
        else:
            if resource_name == 'game_is_not_paused':
                Log.print_log('Game unpaused')
                self.resources[resource_name] = True
            else:
                Log.error_log(f'There is no {resource_name} in kingdom')

    def days_end_resource_reduction(self) -> None:   # Уменьшение по окончании дня
        Log.print_log('Daily reduction')
        for resource in self.resources.keys():
            if resource not in ['game_is_not_paused']:  # black list для вычитания ресурсов
                self.resource_changes(resource, -10)

    def check_game_over_conditions(self) -> bool:  # Возращает True если все ресурсы > 0
        for resource in self.resources.keys():
            if self.resources[resource] < 0:
                return False
        return True

    def __display_resource(self, x=1200, y=100):
        current_resource_ind = 0
        text = ['Армия', 'Казна', 'Здоровье', 'Счастье', 'Религия', '']
        for key in self.resources:
            pyray.draw_text_ex(self.content['fonts']['default_font'], text[current_resource_ind],
                               pyray.Vector2(x, y + 50 * current_resource_ind), 16, 1, raylib.BLACK)
            if current_resource_ind + 1 < len(text):
                pyray.draw_text_ex(self.content['fonts']['default_font'], str(self.resources[key]),
                                   pyray.Vector2(x + 250, y + 50 * current_resource_ind),
                                   16, 1, raylib.BLACK)
            current_resource_ind += 1

    def display(self, queue_result):
        source_rect = pyray.Rectangle(1150, 25, 500, 400)
        texture_rect = pyray.Rectangle(0, 0, 1680, 1323)
        pyray.draw_texture_pro(self.content['static_pictures']['scroll'], texture_rect, source_rect,
                               pyray.Vector2(0, 0), 0, raylib.WHITE)
        self.__display_resource()

        if type(queue_result) is dict:  # Изменение ресурсов королевства
            for resource in self.resources:
                self.resource_changes(resource_name=resource, value=queue_result[resource])
