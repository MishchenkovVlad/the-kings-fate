import pyray
import Python_Objects.Special_Objects.LogPrinter as LogPrinter


def load_image(path):  # Функция загрузки изображения / спрайта
    tmp_image = pyray.load_image(path)
    LogPrinter.print_log_of_load(path)  # Создание лога загрузки файла

    pyray_image = pyray.load_texture_from_image(tmp_image)
    pyray.unload_image(tmp_image)

    return pyray_image


def load_font(path: str = 'resources/fonts/middle_age_font.ttf'):
    codepoints = [0x380 + i if i > 127 else i for i in range(32, 512)]
    pyray_font = pyray.load_font_ex(path, 64, codepoints, 512)
    pyray_font.baseSize = 32

    LogPrinter.print_log_of_load(path)

    return pyray_font


def load_content() -> dict:  # Функция загружающая все необходимые для работы игры изображения, звуки и обхекты
    # Загрузка изображений и звуков
    content = {
        'fonts': {
            'default_font': load_font('resources/fonts/middle_age_font.ttf')
        },
        'music': {
            'main_game_bg_music': 'main_game_bg_music.mp3',
            'main_menu_bg_music': 'main_menu_bg_music.mp3',

        },
        'static_pictures': {
            'main_game_bg': load_image('resources/Images/main_game_background.png'),
            'scroll': load_image('resources/Images/scroll.png'),
            'main_menu_bg': load_image('resources/Images/main_menu_background.png'),
            'game_over_bg': load_image('resources/Images/Game_Over.png')
        },
        'visitors': {
            'knights': {
                'knight_1': load_image('resources/NPCs_sprites/knight_1.png'),
            },
            'merchants': {
                'merchant_1': load_image('resources/NPCs_sprites/merchant_1.png'),
                'merchant_2': load_image('resources/NPCs_sprites/merchant_2.png'),
                'merchant_3': load_image('resources/NPCs_sprites/merchant_3.png'),
                'merchant_4': load_image('resources/NPCs_sprites/merchant_4.png'),
                'merchant_5': load_image('resources/NPCs_sprites/merchant_5.png'),
                'merchant_6': load_image('resources/NPCs_sprites/merchant_6.png'),
                'merchant_7': load_image('resources/NPCs_sprites/merchant_7.png'),
                'merchant_8': load_image('resources/NPCs_sprites/merchant_8.png'),
                'merchant_9': load_image('resources/NPCs_sprites/merchant_9.png'),
            },
            'nobles': {
                'nobleman_1': load_image('resources/NPCs_sprites/merchant_1.png'),
            },
            'peasants': {
                'peasant_1': load_image('resources/NPCs_sprites/peasant_1.png'),
                'peasant_2': load_image('resources/NPCs_sprites/peasant_2.png'),
                'peasant_3': load_image('resources/NPCs_sprites/peasant_3.png'),
                'peasant_4': load_image('resources/NPCs_sprites/peasant_4.png'),
                'peasant_5': load_image('resources/NPCs_sprites/peasant_5.png'),
                'peasant_6': load_image('resources/NPCs_sprites/peasant_6.png'),
                'peasant_7': load_image('resources/NPCs_sprites/peasant_7.png'),
                'peasant_8': load_image('resources/NPCs_sprites/peasant_8.png'),
                'peasant_9': load_image('resources/NPCs_sprites/peasant_9.png'),
                'peasant_10': load_image('resources/NPCs_sprites/peasant_10.png'),
            },
            'priests': {
                'priest_1': load_image('resources/NPCs_sprites/priest_1.png'),
                'priest_2': load_image('resources/NPCs_sprites/priest_2.png'),
                'priest_3': load_image('resources/NPCs_sprites/priest_3.png'),
                'priest_4': load_image('resources/NPCs_sprites/priest_4.png'),
                'priest_5': load_image('resources/NPCs_sprites/priest_5.png'),
                'priest_6': load_image('resources/NPCs_sprites/priest_6.png'),
                'priest_7': load_image('resources/NPCs_sprites/priest_7.png'),
                'priest_8': load_image('resources/NPCs_sprites/priest_8.png'),
                'priest_9': load_image('resources/NPCs_sprites/priest_9.png'),
                'priest_10': load_image('resources/NPCs_sprites/priest_10.png'),
                'priest_11': load_image('resources/NPCs_sprites/priest_11.png'),
            },
            'shamans': {
                'shaman_1': load_image('resources/NPCs_sprites/shaman_1.png')
            },
            'traders': {
                'trader_1': load_image('resources/NPCs_sprites/trader_1.png')
            },
            'ambassador':{
                'ambassador': load_image('resources/NPCs_sprites/peasant_10.png')
            }
        }
    }

    return content
