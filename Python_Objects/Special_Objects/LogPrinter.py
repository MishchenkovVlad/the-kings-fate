import logging

logging.basicConfig(
    level=logging.INFO,
    filename="logs.log",
    filemode="w",
    format="%(asctime)s %(levelname)s %(message)s",
)


def print_log(text="") -> None:  # Функция создающая лог
    logging.info(text)


def error_log(text="") -> None:  # Функция создающая лог ошибки
    logging.error(text)


def print_log_of_load(path="") -> None:  # Функция создающая лог
    msg = "Loaded file: " + path
    print_log(msg)
