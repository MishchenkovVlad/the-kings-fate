import pyray
import raylib
import time


keys_cd = [0, 0]


def text_box(bounds: pyray.Rectangle, text: str, max_length: int, active: bool) -> tuple:

    global keys_cd

    for key_cd_i in range(len(keys_cd)):
        if keys_cd[key_cd_i] != 0:
            keys_cd[key_cd_i] -= 1

    color = raylib.LIGHTGRAY

    if raylib.CheckCollisionPointRec(pyray.get_mouse_position(), bounds):
        if pyray.is_mouse_button_pressed(raylib.MOUSE_BUTTON_LEFT):
            active = not active

    elif pyray.is_mouse_button_pressed(raylib.MOUSE_BUTTON_LEFT):
        active = False

    if active:
        color = raylib.SKYBLUE
        key = pyray.get_char_pressed()

        if key != 0 and len(text) <= max_length:
            if 0x20 <= key <= 0x7e or 0x410 <= key <= 0x44f or key == 0x401 or key == 0x451:
                text += chr(key)

        if pyray.is_key_pressed(raylib.KEY_BACKSPACE):
            text = text[:-1]
            keys_cd[0] = 60

        elif pyray.is_key_down(raylib.KEY_BACKSPACE):
            if keys_cd[0] == 0:
                text = text[:-1]
                keys_cd[0] = 4

        if pyray.is_key_down(raylib.KEY_LEFT_CONTROL) and pyray.is_key_pressed(raylib.KEY_V):
            text += pyray.get_clipboard_text()
            keys_cd[1] = 60

        elif pyray.is_key_down(raylib.KEY_LEFT_CONTROL) and pyray.is_key_down(raylib.KEY_V):
            if keys_cd[1] == 0:
                text += pyray.get_clipboard_text()
                keys_cd[1] = 4

    pyray.draw_rectangle_rec(bounds, color)
    pyray.draw_text_ex(pyray.gui_get_font(), text, pyray.Vector2(bounds.x, bounds.y), 16, 1, raylib.BLACK)

    return text, active
