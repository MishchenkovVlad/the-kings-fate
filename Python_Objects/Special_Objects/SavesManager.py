from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, ForeignKey
from sqlalchemy.orm import mapper, sessionmaker, DeclarativeBase, relationship
from Python_Objects.Static_Objects.Game import Game


def create_connection():
    engine = create_engine('sqlite:///Python_Objects//Special_Objects//Saves.save', echo=False)
    Base.metadata.create_all(engine)
    return engine


class Base(DeclarativeBase):
    pass


class Save(Base):
    __tablename__ = "tbl_save"

    save_id = Column(Integer, primary_key=True)

    name = Column(String(25))
    army = Column(Integer)
    treasury = Column(Integer)
    health = Column(Integer)
    contentment = Column(Integer)
    religion = Column(Integer)


def save(game: Game, name: str) -> None:

    engine = create_connection()
    session = sessionmaker(bind=engine)

    with session() as db:

        game_save = Save(
            name=name,
            army=game.resources.resources['army'],
            treasury=game.resources.resources['treasury'],
            health=game.resources.resources['health'],
            contentment=game.resources.resources['contentment'],
            religion=game.resources.resources['religion']
        )
        db.add(game_save)
        db.commit()


def rewrite_save(slot: int, game: Game, name: str = "") -> None:

    engine = create_connection()
    session = sessionmaker(bind=engine)

    with session() as db:
        game_save = db.query(Save).filter(Save.save_id == slot)[0]
        if name != "":
            game_save.name = name
        game_save.army = game.resources.resources['army']
        game_save.treasury = game.resources.resources['treasury']
        game_save.health = game.resources.resources['health']
        game_save.contentment = game.resources.resources['contentment']
        game_save.religion = game.resources.resources['religion']

        db.commit()


def delete_save(slot: int) -> None:

    engine = create_connection()
    session = sessionmaker(bind=engine)

    with session() as db:
        game_save = db.query(Save).filter(Save.save_id == slot)[0]
        # move_saves = db.query(Save).filter(Save.save_id > slot)

        db.delete(game_save)
        db.commit()

        # for move_save_i in range(move_saves.count() - 1):
        #     move_saves[move_save_i].save_id -= 1

        # db.commit()


def load(slot: int) -> dict:
    engine = create_connection()
    session = sessionmaker(bind=engine)

    loaded_game_resources = {}

    with session() as db:

        if slot == -1:
            slot = db.query(Save).count()

        game_save = db.query(Save).filter(Save.save_id == slot)[0]
        loaded_game_resources["army"] = game_save.army
        loaded_game_resources["treasury"] = game_save.treasury
        loaded_game_resources["health"] = game_save.health
        loaded_game_resources["contentment"] = game_save.contentment
        loaded_game_resources["religion"] = game_save.religion

    return loaded_game_resources


def get_saves() -> list[dict]:

    engine = create_connection()
    session = sessionmaker(bind=engine)

    with session() as db:

        game_saves = db.query(Save).all()

        saves = []
        for game_save in game_saves:
            saves.append({game_save.save_id: str(game_save.name)})

        return saves
