import raylib
import pyray


class Interface:
    def __init__(self, window_width, window_height):
        #  Определение разрешения экрана игры
        self.window_width = window_width
        self.window_height = window_height

        #  Все элементы интерфейса игры
        self.elements = []

    def display(self):  # Функция отображения интерфейса
        for el in self.elements:
            el.display()
