def game_scene(loaded_game):  # Функция вызова итерации игры
    scene_index = loaded_game.update()
    loaded_game.display()

    return scene_index, loaded_game
