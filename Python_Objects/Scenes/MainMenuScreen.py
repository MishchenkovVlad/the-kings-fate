import pyray
import raylib


class MainMenu:
    def __init__(self, content):
        self.__content = content
        #  Инициализация местонахождения и размеров кнопок
        button_continue = {'x': 300, 'y': 200, 'w': 200, 'h': 50}
        button_load = {'x': 300, 'y': 280, 'w': 200, 'h': 50}
        button_settings = {'x': 300, 'y': 360, 'w': 200, 'h': 50}
        button_exit = {'x': 300, 'y': 440, 'w': 200, 'h': 50}

        # Инициализация кнопок как объектов
        self.button_continue = pyray.Rectangle(button_continue['x'],
                                               button_continue['y'],
                                               button_continue['w'],
                                               button_continue['h'])

        self.button_load = pyray.Rectangle(button_load['x'],
                                           button_load['y'],
                                           button_load['w'],
                                           button_load['h'])

        self.button_settings = pyray.Rectangle(button_settings['x'],
                                               button_settings['y'],
                                               button_settings['w'],
                                               button_settings['h'])

        self.button_exit = pyray.Rectangle(button_exit['x'],
                                           button_exit['y'],
                                           button_exit['w'],
                                           button_exit['h'])

    def menu_scene_process_event(self):  # Отрисовка главного экрана
        # Отрисовка картинки заднего фона
        pyray.draw_texture_ex(self.__content['static_pictures']['main_menu_bg'],
                              pyray.Vector2(0, 0), 0,
                              1.6, raylib.WHITE)

        #  Проверка нажатия кнопки и дальнейшие действия, сопутствующие условию
        scene_index = 0
        if pyray.gui_button(self.button_continue, 'Играть'):
            scene_index = 1
        if pyray.gui_button(self.button_load, 'Загрузить'):
            scene_index = 5
        if pyray.gui_button(self.button_settings, 'Настройки'):
            scene_index = 3
        if pyray.gui_button(self.button_exit, 'Выход'):
            scene_index = -1

        return scene_index
