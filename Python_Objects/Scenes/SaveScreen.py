import datetime
import pyray
import raylib

from Python_Objects.Special_Objects.Functions import *
from Python_Objects.Special_Objects.SavesManager import *


class SaveScreen:
    def __init__(self, content: dict):
        self.__content = content
        self.button_add_save = {"x": 225, "y": 125, "w": 250, "h": 50}
        self.button_back = {"x": 225, "y": 600, "w": 250, "h": 50}
        self.save_default = {"x": 225, "y": 200, "w": 250, "h": 50}

        self.__save_name = ""
        self.__text_box_active = False

        self.rect_add_save = pyray.Rectangle(
            self.button_add_save["x"],
            self.button_add_save["y"],
            self.button_add_save["w"],
            self.button_add_save["h"],
        )

        self.rect_button_save = pyray.Rectangle(
            self.button_back["x"],
            self.button_back["y"],
            self.button_back["w"],
            self.button_back["h"],
        )

        self.__storage_list = list()
        self.save_error = False

    def saves_scene_process_event(self, loaded_game):
        pyray.draw_texture_ex(self.__content['static_pictures']['main_menu_bg'],
                              pyray.Vector2(0, 0), 0,
                              1.6, raylib.WHITE)
        scene_index = 2  # Базовое значение
        _today = datetime.date.today()
        _now = datetime.datetime.now()
        save_name = f"{_now.second}.{_now.minute}.{_now.hour}.{_today.day}.{_today.month}.{_today.year}"

        self.__save_name, self.__text_box_active = text_box(pyray.Rectangle(575, 100, 500, 30),
                                                            self.__save_name, 25, self.__text_box_active)

        if self.__save_name != "":
            save_name = self.__save_name

        saves = get_saves()
        saves.reverse()
        if pyray.gui_button(self.rect_add_save, "Добавить сохранение"):
            if len(saves) < 16:
                save(loaded_game, save_name)
            else:
                self.save_error = True

        for game_save_index in range(len(saves)):
            game_save_id = list(saves[game_save_index].keys())[0]
            game_save = saves[game_save_index][game_save_id]
            button_rect = pyray.Rectangle(
                self.save_default["x"] + 320 * ((game_save_index) // 4),
                self.save_default["y"] + 75 * (game_save_index+1) - 300 * ((game_save_index) // 4),
                self.save_default["w"],
                self.save_default["h"],
            )

            if pyray.gui_button(button_rect, game_save):
                rewrite_save(game_save_id, loaded_game, save_name)

        if pyray.gui_button(self.rect_button_save, "Назад"):
            scene_index = 0

        if self.save_error:
            error_rect = pyray.Rectangle(100, 100, 600, 600)
            pyray.draw_rectangle_rec(error_rect, raylib.RED)
            if pyray.gui_button(pyray.Rectangle(300, 300, 250, 50), "Перейти в настройки сохранений"):
                scene_index = 9
                self.save_error = False

        return scene_index
