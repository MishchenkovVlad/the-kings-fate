import pyray
import Python_Objects.Special_Objects.SavesManager as SavesManager
import raylib


class Pause:
    def __init__(self, content: dict):
        self.__content = content

        #  Инициализация расположения и размеров кнопок
        places_and_sizes = {
            "continue": {"x": 300, "y": 200, "w": 200, "h": 50},
            "save": {"x": 300, "y": 280, "w": 200, "h": 50},
            "load": {"x": 300, "y": 360, "w": 200, "h": 50},
            "settings": {"x": 300, "y": 440, "w": 200, "h": 50},
            "main_menu": {"x": 300, "y": 520, "w": 200, "h": 50},
        }

        titles = {
            "continue": "Продолжить",
            "save": "Сохранить",
            "load": "Загрузить",
            "settings": "Настройки",
            "main_menu": "Главное меню",
        }

        functions = {
            "continue": 1,
            "save": 2,
            "settings": 3,
            "load": 5,
            "main_menu": 0,
        }

        self.buttons = dict()
        # Инициализация кнопок как объектов
        for key in places_and_sizes.keys():
            self.buttons[key] = (
                pyray.Rectangle(
                    places_and_sizes[key]["x"],
                    places_and_sizes[key]["y"],
                    places_and_sizes[key]["w"],
                    places_and_sizes[key]["h"],
                ),
                titles[key],
                functions[key],
            )

    def pause_scene_process_event(self):
        pyray.draw_texture_ex(self.__content['static_pictures']['main_menu_bg'],
                              pyray.Vector2(0, 0), 0,
                              1.6, raylib.WHITE)
        scene_index = 4
        for key in self.buttons:
            if pyray.gui_button(*self.buttons[key][0:2]):
                # Функции на кнопки
                function = self.buttons[key][2]
                scene_index = function

        return scene_index
