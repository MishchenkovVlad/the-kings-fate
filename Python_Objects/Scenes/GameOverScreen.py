import pyray


class GameOver:
    def __init__(self):

        #  Инициализация местонахождения и размеров кнопок
        places_and_sizes = {
            "main_menu": {"x": 100, "y": 200, "w": 200, "h": 75},
            "desktop": {"x": 100, "y": 300, "w": 200, "h": 75},
        }

        titles = {
            "main_menu": "Выход в главное меню",
            "desktop": "Выйти на рабочий стол",
        }

        functions = {
            "main_menu": 0,
            "desktop": -1,
        }

        self.buttons = dict()
        # Инициализация кнопок как объектов
        for key in places_and_sizes.keys():
            self.buttons[key] = (
                pyray.Rectangle(
                    places_and_sizes[key]["x"],
                    places_and_sizes[key]["y"],
                    places_and_sizes[key]["w"],
                    places_and_sizes[key]["h"],
                ),
                titles[key],
                functions[key],
            )

    def game_over_scene_process_event(self, content):

        scene_index = 10
        pyray.draw_texture_ex(content['static_pictures']['game_over_bg'],
                              pyray.Vector2(450, 0), 0, 0.77775, pyray.WHITE)
        for key in self.buttons:
            if pyray.gui_button(*self.buttons[key][0:2]):
                scene_index = self.buttons[key][2]

        return scene_index
