import pyray
import raylib

class Settings:
    def __init__(self, content: dict):
        self.__content = content
        self.button_sound = {"x": 300, "y": 200, "w": 200, "h": 50}
        self.button_language = {"x": 300, "y": 280, "w": 200, "h": 50}
        self.button_window_size = {"x": 300, "y": 360, "w": 200, "h": 50}
        self.button_saves_manager = {"x": 300, "y": 440, "w": 200, "h": 50}

        self.button_back = {"x": 300, "y": 600, "w": 200, "h": 50}

        self.button_sound_rect = pyray.Rectangle(
            self.button_sound["x"],
            self.button_sound["y"],
            self.button_sound["w"],
            self.button_sound["h"],
        )

        self.button_language_rect = pyray.Rectangle(
            self.button_language["x"],
            self.button_language["y"],
            self.button_language["w"],
            self.button_language["h"],
        )

        self.button_window_size_rect = pyray.Rectangle(
            self.button_window_size["x"],
            self.button_window_size["y"],
            self.button_window_size["w"],
            self.button_window_size["h"],
        )

        self.button_back = pyray.Rectangle(
            self.button_back["x"],
            self.button_back["y"],
            self.button_back["w"],
            self.button_back["h"],
        )

        self.button_saves_manager_rect = pyray.Rectangle(
            self.button_saves_manager["x"],
            self.button_saves_manager["y"],
            self.button_saves_manager["w"],
            self.button_saves_manager["h"],
        )

    def settings_scene_process_event(self):
        pyray.draw_texture_ex(self.__content['static_pictures']['main_menu_bg'],
                              pyray.Vector2(0, 0), 0,
                              1.6, raylib.WHITE)
        scene_index = 3  # Базовое значение

        if pyray.gui_button(self.button_sound_rect, "Sound Effects"):
            scene_index = 6  # Звуковые эфекты
        if pyray.gui_button(self.button_language_rect, "Language"):
            scene_index = 7  # Язык ???
        if pyray.gui_button(self.button_window_size_rect, "Window Size"):
            scene_index = 8  # Пропорции экрана
        if pyray.gui_button(self.button_saves_manager_rect, "Настройка сохранений"):
            scene_index = 9  # Удаление и переименовка сохранений
        if pyray.gui_button(self.button_back, "Назад"):
            scene_index = 0  # Возращение в главное меню

        return scene_index


#
# def main():
#
#     scene_index = 4
#     scene_changed = True
#
#     width = 800
#     height = 600
#     background_color = pyray.BLACK
#
#     pyray.init_window(width, height, "Game")
#
#     SettingsScene = Settings()
#
#     while not pyray.window_should_close():
#         if scene_changed:
#             scene_changed = False
#
#         if not scene_changed:
#             if scene_index == 4:  # menu
#                 scene_changed, scene_index = SettingsScene.settings_scene_process_event(scene_changed, scene_index)
#
#         if not scene_changed:
#             pyray.begin_drawing()
#             pyray.clear_background(background_color)
#             if scene_index == 4:
#                 SettingsScene.draw_buttons()
#
#             pyray.end_drawing()
#
#     pyray.close_window()
#
#
# if __name__ == '__main__':
#     main()
