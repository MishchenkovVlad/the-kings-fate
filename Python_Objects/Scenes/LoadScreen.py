import pyray
import raylib
from Python_Objects.Special_Objects.SavesManager import *


class Load:
    def __init__(self, content: dict):
        self.back_button: dict = {"x": 300, "y": 600, "w": 200, "h": 50}
        self.back_button_rect = pyray.Rectangle(
            self.back_button["x"],
            self.back_button["y"],
            self.back_button["w"],
            self.back_button["h"]
        )
        self.save_default = {"x": 225, "y": 200, "w": 250, "h": 50}
        self.__content = content

    def load_scene_process_event(self, loaded_game: Game):
        pyray.draw_texture_ex(self.__content['static_pictures']['main_menu_bg'],
                              pyray.Vector2(0, 0), 0,
                              1.6, raylib.WHITE)
        scene_index = 5

        saves = get_saves()
        saves.reverse()
        for game_save_index in range(len(saves)):
            game_save_id = list(saves[game_save_index].keys())[0]
            game_save = saves[game_save_index][game_save_id]
            button_rect = pyray.Rectangle(
                self.save_default["x"] + 320 * game_save_index // 4,
                self.save_default["y"] + 75 * (game_save_index + 1) - 300 * (game_save_index // 4),
                self.save_default["w"],
                self.save_default["h"],
            )

            if pyray.gui_button(button_rect, game_save):
                loaded_game.resources.resources = load(game_save_id)
                scene_index = 1

        if pyray.gui_button(self.back_button_rect, "Назад"):
            scene_index = 0

        return scene_index, loaded_game

