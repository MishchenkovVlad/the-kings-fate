#  Инициализация библиотек для работы
import pyray
import raylib

#  Инициализация файлов отдельных окон
import Python_Objects.Scenes.GameScene as GameScene
import Python_Objects.Scenes.MainMenuScreen as MainMenuScreen
import Python_Objects.Static_Objects.Game as Game
import Python_Objects.Scenes.SaveScreen as SaveScreen
import Python_Objects.Scenes.SettingsScreen as SettingsScreen
import Python_Objects.Scenes.PauseScreen as PauseScreen
import Python_Objects.Scenes.LoadScreen as LoadScreen
import Python_Objects.Scenes.Settings.SoundSettings as SoundSettings
import Python_Objects.Scenes.Settings.LanguageSettings as LanguageSettings
import Python_Objects.Scenes.Settings.WindowSizeSettings as WindowSizeSettings
import Python_Objects.Scenes.Settings.SavesSettings as SavesSettings

#  Инициализация функций для работы игры
import Python_Objects.Special_Objects.ContentLoader as ContentLoader
from Python_Objects.Scenes.GameOverScreen import GameOver


def main():
    # Инициализация окна
    window_width = 1600
    window_height = 800

    pyray.init_window(window_width, window_height, "King's Fate")
    pyray.set_exit_key(pyray.KeyboardKey.KEY_F8)
    pyray.set_target_fps(120)
    pyray.init_audio_device()
    background_color = raylib.BLACK

    content = ContentLoader.load_content()  # Загрузка изображений и звуков
    pyray.gui_set_font(content['fonts']['default_font'])  # Установка стандартного шрифат

    # Инициализация классов (там где нужно)
    main_menu = MainMenuScreen.MainMenu(content=content)
    save_screen = SaveScreen.SaveScreen(content=content)
    settings_screen = SettingsScreen.Settings(content=content)
    game_over_scene = GameOver()
    pause_screen = PauseScreen.Pause(content=content)
    load_screen = LoadScreen.Load(content=content)

    setup_saves_screen = SavesSettings.SetupSaves(content=content)

    '''
    scene_index может иметь следующие значения
    0 - экран меню
    1 - экран игры
    2 - экран паузы
    3 - экран проигрыша
    4 - экран паузы
    5 - экран загрузки
    6 - экран настройки звука
    7 - экран настройки языка
    8 - экран настройки окна
    9 - экран настройки сохранений
    10 - экран проигрыша
    '''

    content = ContentLoader.load_content()  # Загрузка изображений и звуков
    pyray.gui_set_font(content['fonts']['default_font'])  # Установка стандартного шрифта
    pyray.gui_set_style(raylib.DEFAULT, raylib.TEXT_COLOR_NORMAL, 0x000000FF)  # Настройка шрифта
    loaded_game = Game.Game(content, window_width, window_height)  # Инициализация новой игры

    # Инициализация переменных
    scene_index = 0

    while not pyray.window_should_close() and scene_index >= 0:
        pyray.begin_drawing()
        pyray.clear_background(background_color)

        if scene_index == 0:
            # Функция отрисовки меню
            scene_index = main_menu.menu_scene_process_event()

        elif scene_index == 1:
            # Функция отрисовки экрана окна
            scene_index, loaded_game = GameScene.game_scene(loaded_game)

        elif scene_index == 2:
            # Функция отрисовки сохранений
            scene_index = save_screen.saves_scene_process_event(loaded_game)

        elif scene_index == 3:
            # Функция отрисовки настроек
            scene_index = settings_screen.settings_scene_process_event()

        elif scene_index == 4:
            # Функция отрисовки паузы
            scene_index = pause_screen.pause_scene_process_event()

        elif scene_index == 5:
            # Функция отрисовки загрузок
            scene_index, loaded_game = load_screen.load_scene_process_event(loaded_game)

        elif scene_index == 6:
            # Функция настройки звука
            scene_index = 0

        elif scene_index == 7:
            # Функция настройки языка
            scene_index = 0

        elif scene_index == 8:
            # Функция настройки окна
            scene_index = 0

        elif scene_index == 9:
            # Функция настройки сохранений
            scene_index = setup_saves_screen.setup_saves_scene_process_event()

        elif scene_index == 10:
            # Функция отрисовки эрана проигрыша
            scene_index = game_over_scene.game_over_scene_process_event(content=content)
            if scene_index != 10:
                loaded_game = Game.Game(content, window_width, window_height)  # Инициализация новой игры

        pyray.end_drawing()

    pyray.close_window()


if __name__ == '__main__':
    main()
